MODULE POTENTIALS 
      IMPLICIT NONE
      CONTAINS

SUBROUTINE COULOMBALL(Vf1)
        ! Interaction between e and other e's
        USE OMP_LIB 
        USE DOUBLE
        USE acommondata, ONLY: szcell, ndop, cellc, fact
        USE arrays1, ONLY: NEGA
        IMPLICIT NONE
        INTEGER    p, q
        REAL (KIND=DP)     Vf1
        REAL (KIND=DP)     RXIJ, RYIJ, RZIJ
        REAL (KIND=DP)     RIJ, VIJ
        Vf1 = 0.d0
!$OMP PARALLEL SHARED(ndop, NEGA) PRIVATE(p, q, RXIJ, RYIJ, RZIJ, RIJ, VIJ) REDUCTION (+: Vf1) 
!$OMP DO SCHEDULE(AUTO)
        DO  p = 1, ndop - 1
            DO q = p + 1, ndop
                 RXIJ = NEGA(p)%PDX - NEGA(q)%PDX
                 RYIJ = NEGA(p)%PDY - NEGA(q)%PDY
                 RZIJ = NEGA(p)%PDZ - NEGA(q)%PDZ
                 RXIJ = RXIJ - ANINT ( RXIJ )
                 RYIJ = RYIJ - ANINT ( RYIJ )
                 RZIJ = RZIJ - ANINT ( RZIJ )
                 RIJ  = SQRT ( RXIJ * RXIJ + RYIJ * RYIJ + RZIJ * RZIJ )
!                 VIJ  = NEGA(p)%Zch * NEGA(q)%Zch / RIJ
                 VIJ  = 1 / RIJ
                 Vf1  = Vf1 + VIJ
             END DO
        END DO
!$OMP END DO
!$OMP END PARALLEL
        Vf1 =  fact*Vf1/cellc/szcell
        RETURN
END SUBROUTINE COULOMBALL

SUBROUTINE COULOMBALLAD(Vf)
        !Interaction between e and other e's
        !The upper limitd is now ndop + 1
        USE DOUBLE
        USE acommondata, ONLY: szcell, ndop, cellc, fact
        USE arrays1, ONLY: NEGA
        IMPLICIT NONE
        INTEGER    p, q
        REAL (KIND=DP)     RXIJ, RYIJ, RZIJ, Vf
        REAL (KIND=DP)     RIJ, VIJ
        Vf = 0.d0
!$OMP PARALLEL SHARED(ndop, NEGA) PRIVATE(p, q, RXIJ, RYIJ, RZIJ, RIJ, VIJ) REDUCTION (+: Vf)
!$OMP DO SCHEDULE(AUTO)
        DO  p = 1, ndop
            DO q = p + 1, ndop + 1
                 RXIJ = NEGA(p)%PDX - NEGA(q)%PDX
                 RYIJ = NEGA(p)%PDY - NEGA(q)%PDY
                 RZIJ = NEGA(p)%PDZ - NEGA(q)%PDZ
                 RXIJ = RXIJ - ANINT ( RXIJ )
                 RYIJ = RYIJ - ANINT ( RYIJ )
                 RZIJ = RZIJ - ANINT ( RZIJ )
                 RIJ  = SQRT ( RXIJ * RXIJ + RYIJ * RYIJ + RZIJ * RZIJ )
!                 VIJ  = NEGA(p)%Zch * NEGA(q)%Zch / RIJ
                 VIJ  = 1 / RIJ
                 Vf  = Vf + VIJ
             END DO
        END DO
!$OMP END DO
!$OMP END PARALLEL
        Vf =  fact*Vf/cellc/szcell
        RETURN
END SUBROUTINE COULOMBALLAD

SUBROUTINE COULOMBALLRE(Vf1)
        ! Interaction between e and other e's
        USE OMP_LIB 
        USE DOUBLE
        USE acommondata, ONLY: szcell, ndop, cellc, fact
        USE arrays1, ONLY: NEGA
        IMPLICIT NONE
        INTEGER    p, q
        REAL (KIND=DP)     RXIJ, RYIJ, RZIJ, Vf1
        REAL (KIND=DP)     RIJ, VIJ
        Vf1 = 0.d0
!$OMP PARALLEL SHARED(ndop, NEGA) PRIVATE(p, q, RXIJ, RYIJ, RZIJ, RIJ, VIJ) REDUCTION (+: Vf1) 
!$OMP DO SCHEDULE(AUTO)
        DO  p = 1, ndop - 1 
            DO q = p + 1, ndop
                 RXIJ = NEGA(p)%PDX - NEGA(q)%PDX
                 RYIJ = NEGA(p)%PDY - NEGA(q)%PDY
                 RZIJ = NEGA(p)%PDZ - NEGA(q)%PDZ
                 RXIJ = RXIJ - ANINT ( RXIJ )
                 RYIJ = RYIJ - ANINT ( RYIJ )
                 RZIJ = RZIJ - ANINT ( RZIJ )
                 RIJ  = SQRT ( RXIJ * RXIJ + RYIJ * RYIJ + RZIJ * RZIJ )
                 VIJ  = NEGA(p)%Zch*NEGA(q)%Zch / RIJ
                 Vf1  = Vf1 + VIJ
             END DO
        END DO
!$OMP END DO
!$OMP END PARALLEL
        Vf1 =  fact*Vf1/cellc/szcell
        RETURN
END SUBROUTINE COULOMBALLRE

SUBROUTINE COULOMB(f1, Vf1 )
        ! Interaction between e and other e's
        USE DOUBLE
        USE acommondata, ONLY: fact, szcell, ndop, cellc
        USE arrays1, ONLY: NEGA
        IMPLICIT NONE
        INTEGER    f1, p 
        REAL (KIND=DP)     RXIJ, RYIJ, RZIJ, Vf1
        REAL (KIND=DP)     RIJ, VIJ
        Vf1 = 0.d0
        DO  p = 1, ndop
            IF(p .NE. f1) THEN
                 RXIJ = NEGA(p)%PDX - NEGA(f1)%PDX
                 RYIJ = NEGA(p)%PDY - NEGA(f1)%PDY
                 RZIJ = NEGA(p)%PDZ - NEGA(f1)%PDZ
                 RXIJ = RXIJ - ANINT ( RXIJ )
                 RYIJ = RYIJ - ANINT ( RYIJ )
                 RZIJ = RZIJ - ANINT ( RZIJ )
                 RIJ  = SQRT ( RXIJ * RXIJ + RYIJ * RYIJ + RZIJ * RZIJ )
!                 VIJ  = NEGA(p)%Zch * NEGA(f1)%Zch / RIJ
                 VIJ  = 1 / RIJ
                 Vf1  = Vf1 + VIJ
             END IF
        END DO
        Vf1 =  fact*Vf1/cellc/szcell
        RETURN
END SUBROUTINE COULOMB

SUBROUTINE COULOMBRE(f1, Vf1 )
        ! Interaction between e and other e's
        USE DOUBLE
        USE acommondata, ONLY: szcell, ndop, cellc
        USE arrays1, ONLY: NEGA
        IMPLICIT NONE
        INTEGER    f1, p
        REAL (KIND=DP)     RXIJ, RYIJ, RZIJ, Vf1
        REAL (KIND=DP)     RIJ, VIJ
        Vf1 = 0.d0
        DO  p = 1, ndop - 1 ! Shit upper limit
            IF(p .NE. f1) THEN
!                 print*, p, f1, ndop, NEGA(p)%PDX, NEGA(p)%PDX
                 RXIJ = NEGA(p)%PDX - NEGA(f1)%PDX
                 RYIJ = NEGA(p)%PDY - NEGA(f1)%PDY
                 RZIJ = NEGA(p)%PDZ - NEGA(f1)%PDZ
                 RXIJ = RXIJ - ANINT ( RXIJ )
                 RYIJ = RYIJ - ANINT ( RYIJ )
                 RZIJ = RZIJ - ANINT ( RZIJ )
                 RIJ  = SQRT ( RXIJ * RXIJ + RYIJ * RYIJ + RZIJ * RZIJ )
!                 VIJ  = NEGA(p)%Zch * NEGA(f1)%Zch / RIJ
                 VIJ  = 1 / RIJ
                 Vf1  = Vf1 + VIJ
             END IF
        END DO
        Vf1 =  Vf1/cellc/szcell
        RETURN
END SUBROUTINE COULOMBRE

SUBROUTINE C60L(f1)
        ! Interaction between e and other e's remove the contribution from f1
        USE DOUBLE
        USE acommondata, ONLY: szcell, ndop, cellc, fact
        USE arrays1, ONLY: NEGA
        IMPLICIT NONE 
        INTEGER    f1, p
        REAL (KIND=DP) :: RXIJ, RYIJ, RZIJ
        REAL (KIND=DP) :: RIJ
        DO  p = 1, ndop
            IF(p .NE. f1) THEN
!                 print*, p, f1, ndop
!                 print*, NEGA(p)%PDX,  NEGA(f1)%PDX
                 RXIJ = NEGA(p)%PDX - NEGA(f1)%PDX
                 RYIJ = NEGA(p)%PDY - NEGA(f1)%PDY
                 RZIJ = NEGA(p)%PDZ - NEGA(f1)%PDZ
                 RXIJ = RXIJ - ANINT ( RXIJ )
                 RYIJ = RYIJ - ANINT ( RYIJ )
                 RZIJ = RZIJ - ANINT ( RZIJ )
                 RIJ  = SQRT ( RXIJ * RXIJ + RYIJ * RYIJ + RZIJ * RZIJ )
!                 WRITE(15,*)  NEGA(p)%Zch ,  NEGA(f1)%Zch
                 NEGA(p)%enc60  = NEGA(p)%enc60 - fact/RIJ/cellc/szcell
             END IF
        END DO
        RETURN
END SUBROUTINE C60L

SUBROUTINE C60M(f1)
        ! Interaction between e and other e's add new contribution from f1
        USE DOUBLE
        USE acommondata, ONLY: szcell, ndop, cellc, fact
        USE arrays1, ONLY: NEGA
        IMPLICIT NONE
        INTEGER    f1, p
        REAL (KIND=DP) :: RXIJ, RYIJ, RZIJ
        REAL (KIND=DP) :: RIJ
        DO  p = 1, ndop
            IF(p .NE. f1) THEN
                 RXIJ = NEGA(p)%PDX - NEGA(f1)%PDX
                 RYIJ = NEGA(p)%PDY - NEGA(f1)%PDY
                 RZIJ = NEGA(p)%PDZ - NEGA(f1)%PDZ
                 RXIJ = RXIJ - ANINT ( RXIJ )
                 RYIJ = RYIJ - ANINT ( RYIJ )
                 RZIJ = RZIJ - ANINT ( RZIJ )
                 RIJ  = SQRT ( RXIJ * RXIJ + RYIJ * RYIJ + RZIJ * RZIJ )
                 NEGA(p)%enc60  = NEGA(p)%enc60 + fact/RIJ/cellc/szcell
             END IF
        END DO
        RETURN
END SUBROUTINE C60M

SUBROUTINE C60REL(f1)
        ! Interaction between e and other e's remove the contribution from f1
        USE DOUBLE
        USE acommondata, ONLY: szcell, ndop, cellc, fact
        USE arrays1, ONLY: NEGA
        IMPLICIT NONE
        INTEGER    f1, p
        REAL (KIND=DP) :: RXIJ, RYIJ, RZIJ
        REAL (KIND=DP) :: RIJ
        DO  p = 1, ndop - 1  !Shift upper limit
            IF(p .NE. f1) THEN
                 RXIJ = NEGA(p)%PDX - NEGA(f1)%PDX
                 RYIJ = NEGA(p)%PDY - NEGA(f1)%PDY
                 RZIJ = NEGA(p)%PDZ - NEGA(f1)%PDZ
                 RXIJ = RXIJ - ANINT ( RXIJ )
                 RYIJ = RYIJ - ANINT ( RYIJ )
                 RZIJ = RZIJ - ANINT ( RZIJ )
                 RIJ  = SQRT ( RXIJ * RXIJ + RYIJ * RYIJ + RZIJ * RZIJ )
                 NEGA(p)%enc60  = NEGA(p)%enc60 - fact/RIJ/cellc/szcell
             END IF
        END DO
        RETURN
END SUBROUTINE C60REL

SUBROUTINE C60REM(f1)
        ! Interaction between e and other e's add new contribution from f1
        USE DOUBLE
        USE acommondata, ONLY: szcell, ndop, cellc, fact
        USE arrays1, ONLY: NEGA
        IMPLICIT NONE
        INTEGER    f1, p
        REAL (KIND=DP) :: RXIJ, RYIJ, RZIJ
        REAL (KIND=DP) :: RIJ
        DO  p = 1, ndop - 1  !Shift upper limit
            IF(p .NE. f1) THEN
                 RXIJ = NEGA(p)%PDX - NEGA(f1)%PDX
                 RYIJ = NEGA(p)%PDY - NEGA(f1)%PDY
                 RZIJ = NEGA(p)%PDZ - NEGA(f1)%PDZ
                 RXIJ = RXIJ - ANINT ( RXIJ )
                 RYIJ = RYIJ - ANINT ( RYIJ )
                 RZIJ = RZIJ - ANINT ( RZIJ )
                 RIJ  = SQRT ( RXIJ * RXIJ + RYIJ * RYIJ + RZIJ * RZIJ )
                 NEGA(p)%enc60  = NEGA(p)%enc60 + fact/RIJ/cellc/szcell
             END IF
        END DO
        RETURN
END SUBROUTINE C60REM

SUBROUTINE DOPELECT_POT(f1, Vf2 )
        !! Interaction0 between e and dopes"
        USE OMP_LIB
        USE DOUBLE
        USE acommondata, ONLY: ndop, convl, convHeV, cellc, szcell, epsble, bb, sig, en0 
        USE arrays1, ONLY: HOLE, NEGA
        IMPLICIT NONE
        INTEGER    f1, p
        REAL (KIND=DP)     RXIJ, RYIJ, RZIJ, VF2
        REAL (KIND=DP)     RIJ, VIJ, convu, convs, ratio, bbb, ssig
!        en0   = 0.634*(epsc60/epsble) 
        Vf2   = 0.d0
        convu = convl*convHeV
        convs = szcell*cellc 
        ratio = convu/epsble
        bbb   = bb
        ssig  = sig
!$OMP PARALLEL SHARED(HOLE, NEGA, f1, ndop, convs, ratio, bbb, ssig, en0) PRIVATE(p, RXIJ, RYIJ, RZIJ, RIJ, VIJ)
!$OMP DO REDUCTION (+: Vf2) SCHEDULE(AUTO) 
        DO  p = 1, ndop
              RXIJ = HOLE(p)%PDX - NEGA(f1)%PDX
              RYIJ = HOLE(p)%PDY - NEGA(f1)%PDY
              RZIJ = HOLE(p)%PDZ - NEGA(f1)%PDZ
              RXIJ = RXIJ - ANINT ( RXIJ )
              RYIJ = RYIJ - ANINT ( RYIJ )
              RZIJ = RZIJ - ANINT ( RZIJ )
              RIJ  = convs*SQRT ( RXIJ * RXIJ + RYIJ * RYIJ + RZIJ * RZIJ )
              VIJ  = -en0 + (en0 -  ratio/RIJ)/(1 + exp((bbb-RIJ)/ssig))
              Vf2  = Vf2 + VIJ
         END DO
!$OMP END DO   
!$OMP END PARALLEL
        RETURN
END SUBROUTINE DOPELECT_POT 

SUBROUTINE DOPELECT_POTALL(VP)
        !! Interaction0 between e and dopes"
        USE OMP_LIB
        USE DOUBLE
        USE acommondata, ONLY: ndop, convl, convHeV, cellc, szcell, epsble, bb, sig, en0
        USE arrays1, ONLY: HOLE, NEGA
        IMPLICIT NONE
        INTEGER  p,  q
        REAL (KIND=DP)     RXIJ, RYIJ, RZIJ, ratio, VP
        REAL (KIND=DP)     RIJ, VIJ, convu, convs, bbb, ssig
!        en0   = 0.634*(epsc60/epsble) 
        convu = convl*convHeV
        convs = szcell*cellc
        ratio = convu/epsble
        bbb   = bb
        ssig  = sig
        VP = 0.d0
!$OMP PARALLEL SHARED(HOLE, NEGA, ndop, convs, ratio, bbb, ssig, en0) PRIVATE(p, q, RXIJ, RYIJ, RZIJ, RIJ, VIJ)
!$OMP DO REDUCTION (+: VP) SCHEDULE(AUTO)
        DO  q = 1, ndop
            DO p = 1, ndop
               RXIJ = HOLE(p)%PDX - NEGA(q)%PDX
               RYIJ = HOLE(p)%PDY - NEGA(q)%PDY
               RZIJ = HOLE(p)%PDZ - NEGA(q)%PDZ
               RXIJ = RXIJ - ANINT ( RXIJ )
               RYIJ = RYIJ - ANINT ( RYIJ )
               RZIJ = RZIJ - ANINT ( RZIJ )
               RIJ  = convs*SQRT ( RXIJ * RXIJ + RYIJ * RYIJ + RZIJ * RZIJ )
               VIJ  = (-en0 + (en0 -  ratio/RIJ)/(1 + exp((bb-RIJ)/sig)))
               VP   = VP + VIJ
             END DO
         END DO
!$OMP END DO   
!$OMP END PARALLEL
        RETURN
END SUBROUTINE DOPELECT_POTALL

SUBROUTINE DOPELECT_POTALLAD(VP)
        !! Interaction0 between e and dopes"
        USE OMP_LIB
        USE DOUBLE
        USE acommondata, ONLY: ndop, convl, convHeV, cellc, szcell, epsble, bb, sig, en0
        USE arrays1, ONLY: HOLE, NEGA
        IMPLICIT NONE
        INTEGER    p, q
        REAL (KIND=DP)     RXIJ, RYIJ, RZIJ, val, ratio, VP
        REAL (KIND=DP)     RIJ, VIJ, convu, convs, bbb, ssig
!        en0   = 0.634*(epsc60/epsble) 
        convu = convl*convHeV
        convs = szcell*cellc
        bbb   = bb
        ssig  = sig
        ratio = convu/epsble
        VP = 0.d0
!$OMP PARALLEL SHARED(HOLE, NEGA, ndop, convs, ratio, bbb, ssig, en0) PRIVATE(p, q, RXIJ, RYIJ, RZIJ, RIJ, VIJ)
!$OMP DO REDUCTION (+: VP) SCHEDULE(AUTO)
        DO  q = 1, ndop + 1
            val = NEGA(q)%Zch*NEGA(q)%Zch
            DO p = 1, ndop
               RXIJ = HOLE(p)%PDX - NEGA(q)%PDX
               RYIJ = HOLE(p)%PDY - NEGA(q)%PDY
               RZIJ = HOLE(p)%PDZ - NEGA(q)%PDZ
               RXIJ = RXIJ - ANINT ( RXIJ )
               RYIJ = RYIJ - ANINT ( RYIJ )
               RZIJ = RZIJ - ANINT ( RZIJ )
               RIJ  = convs*SQRT ( RXIJ * RXIJ + RYIJ * RYIJ + RZIJ * RZIJ )
               VIJ  = val*(-en0 + (en0 -  ratio/RIJ)/(1 + exp((bbb-RIJ)/ssig)))
               VP   = VP + VIJ
             END DO
         END DO
!$OMP END DO   
!$OMP END PARALLEL
        RETURN
END SUBROUTINE DOPELECT_POTALLAD

SUBROUTINE DOPELECT_POTALLRE(VP)
        !! Interaction0 between e and dopes"
        USE OMP_LIB
        USE DOUBLE
        USE acommondata, ONLY: ndop, convl, convHeV, cellc, szcell, epsble, bb, sig, en0
        USE arrays1, ONLY: HOLE, NEGA
        IMPLICIT NONE
        INTEGER  p,  q
        REAL (KIND=DP)     RXIJ, RYIJ, RZIJ, ratio, VP
        REAL (KIND=DP)     RIJ, VIJ, convu, convs, bbb, ssig
!        en0   = 0.634*(epsc60/epsble) 
        convu = convl*convHeV
        convs = szcell*cellc
        ratio = convu/epsble
        bbb   = bb
        ssig  = sig
        VP = 0.d0
!$OMP PARALLEL SHARED(HOLE, NEGA, ndop, convs, ratio, bbb, ssig, en0) PRIVATE(p, q, RXIJ, RYIJ, RZIJ, RIJ, VIJ)
!$OMP DO REDUCTION (+: VP) SCHEDULE(AUTO)
        DO  q = 1, ndop - 1        !Shift upper limit
            DO p = 1, ndop
               RXIJ = HOLE(p)%PDX - NEGA(q)%PDX
               RYIJ = HOLE(p)%PDY - NEGA(q)%PDY
               RZIJ = HOLE(p)%PDZ - NEGA(q)%PDZ
               RXIJ = RXIJ - ANINT ( RXIJ )
               RYIJ = RYIJ - ANINT ( RYIJ )
               RZIJ = RZIJ - ANINT ( RZIJ )
               RIJ  = convs*SQRT ( RXIJ * RXIJ + RYIJ * RYIJ + RZIJ * RZIJ )
               VIJ  = -1*NEGA(q)%Zch*(-en0 + (en0 -  ratio/RIJ)/(1 + exp((bb-RIJ)/sig)))
               VP   = VP + VIJ
             END DO
         END DO
!$OMP END DO   
!$OMP END PARALLEL
        RETURN
END SUBROUTINE DOPELECT_POTALLRE

SUBROUTINE FIXPOS(VR)
!       All dopant-dopant interation (Remains contants if they keep in the same site)       
        USE OMP_LIB 
        USE DOUBLE
        USE acommondata, ONLY: ndop, szcell, cellc, fact  
        USE arrays1, ONLY: HOLE 
        IMPLICIT NONE
        INTEGER    I, J  
        REAL (KIND=DP)  RXIJ, RYIJ, RZIJ
        REAL (KIND=DP)  RIJ, VIJ, VR
        VR = 0.d0
!$OMP PARALLEL SHARED(HOLE, ndop) PRIVATE(I, J, RXIJ, RYIJ, RZIJ, RIJ, VIJ) REDUCTION (+: VR)
!$OMP DO SCHEDULE(AUTO)
        DO  I = 1, ndop  - 1
            DO  J = I + 1, ndop
                 RXIJ = HOLE(J)%PDX - HOLE(I)%PDX
                 RYIJ = HOLE(J)%PDY - HOLE(I)%PDY
                 RZIJ = HOLE(J)%PDZ - HOLE(I)%PDZ
                 RXIJ = RXIJ - ANINT ( RXIJ )
                 RYIJ = RYIJ - ANINT ( RYIJ )
                 RZIJ = RZIJ - ANINT ( RZIJ )
                 RIJ  = SQRT ( RXIJ * RXIJ + RYIJ * RYIJ + RZIJ * RZIJ )
!                 VIJ  = HOLE(I)%Zch * HOLE(J)%Zch / RIJ
                 VIJ  = 1 / RIJ 
                 VR   = VR + VIJ
            END DO
         END DO
!$OMP END DO   
!$OMP END PARALLEL
        VR = fact*VR/szcell/cellc
        RETURN
END SUBROUTINE FIXPOS 

END MODULE POTENTIALS 
