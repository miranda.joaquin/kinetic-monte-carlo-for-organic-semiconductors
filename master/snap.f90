SUBROUTINE SNAP(step)
        USE DOUBLE
        USE arrays1, ONLY: NEGA, HOLE
        USE RANDOM
        USE acommondata, ONLY: cellc, ndop, ldop, scal
        IMPLICIT NONE
        INTEGER             :: I, step
        CHARACTER(len=1024) :: filename 
        WRITE (filename, "(I5,A4, f3.2, A1, I2, A4)") step, "conf", ldop, "l", cellc, ".xyz"
        
        OPEN (UNIT=3, FILE=filename, STATUS='NEW', ACTION='WRITE')
        WRITE(3, *) ndop 
        WRITE(3, *) "   "

        DO I = 1, ndop
!            IF (flu(I)%Zch .EQ. 0) THEN
!                mol = "F"
!            ELSE IF (flu(I)%Zch .EQ. 1) THEN
!                mol = "H"
!            ELSE 
!                mol = "O"
!            END IF
            write(3, '(A2, 2x,F15.7, 2x,F15.7,2x, F15.7)')  "O", scal*NEGA(I)%PDX, scal*NEGA(I)%PDY, scal*NEGA(I)%PDZ
            write(3, '(A2, 2x,F15.7, 2x,F15.7,2x, F15.7)')  "H", scal*HOLE(I)%PDX, scal*HOLE(I)%PDY, scal*HOLE(I)%PDZ
        END DO

        CLOSE(UNIT=3)

END SUBROUTINE SNAP

