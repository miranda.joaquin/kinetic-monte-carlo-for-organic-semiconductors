SUBROUTINE SNAPALL(step)
        USE DOUBLE
        USE arrays1, ONLY: flu
        USE RANDOM
        USE acommondata, ONLY: cellc, npartd, ldop, scal
        IMPLICIT NONE
        INTEGER             :: I, step
        CHARACTER(len=1024) :: filename1
        CHARACTER           :: mol
        WRITE (filename1, "(I4,A5, f3.2, A1, I2, A4)") step, "posit", ldop, "l", cellc, ".xyz"

        OPEN (UNIT=1, FILE=filename1, STATUS='NEW', ACTION='WRITE')
        WRITE(1, *) npartd 
        WRITE(1, *) "   "

        DO I = 1, npartd
            IF (flu(I)%Zch .EQ. 0) THEN
                mol = "F"
            ELSE IF (flu(I)%Zch .EQ. 1) THEN
                mol = "H"
            ELSE 
                mol = "O"
            END IF
            write(1, '(A2, 3x,F15.5, 3x,F15.5,3x, F15.5)')  mol, scal*flu(I)%PDX, scal*flu(I)%PDY, scal*flu(I)%PDZ
        END DO

        CLOSE(UNIT=1)

END SUBROUTINE SNAPALL

