MODULE CLASS_FREQS
    USE DOUBLE
    IMPLICIT NONE

    TYPE, PUBLIC :: sumratios

    INTEGER         :: ncharge     !Charge to move
    INTEGER         :: hopnn       !nearest nn label to move
    REAL (KIND=DP)  :: nnratio     !Contains waiting time
    REAL (KIND=DP)  :: dx
    END TYPE sumratios 

END MODULE CLASS_FREQS
