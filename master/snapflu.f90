SUBROUTINE SNAPFLU(step)
        USE DOUBLE
        USE arrays1, ONLY: NEGA, HOLE, NEUT
        USE acommondata, ONLY: cellc, ndop, npartd, ldop, scal
        IMPLICIT NONE
        INTEGER             :: I, step
        CHARACTER(len=1024) :: filename1

        WRITE (filename1, "(I4,A5, f3.2, A1, I2, A4)") step, "flues", ldop, "l", cellc, ".out"
        OPEN (UNIT=2, FILE=filename1, STATUS='NEW', ACTION='WRITE')

        DO I = 1,ndop 
            write(2, '(I2)') NEGA(I)%Zch
            write(2, '(12I8)') NEGA(I)%FN1
            write(2, '(I8)') NEGA(I)%site
            write(2, '(3F14.8)') NEGA(I)%PDX, NEGA(I)%PDY, NEGA(I)%PDZ
            write(2, '(2D17.9)') NEGA(I)%enc60, NEGA(I)%endop
            write(2, *) NEGA(I)%RW
            write(2, '(2D17.9)') NEGA(I)%dx, NEGA(I)%dt
            write(2, '(D16.8)') NEGA(I)%hop  
        END DO

        DO I = 1,ndop
            write(2, '(I2)') HOLE(I)%Zch
            write(2, '(12I8)') HOLE(I)%FN1
            write(2, '(I8)') HOLE(I)%site
            write(2, '(3F14.8)') HOLE(I)%PDX, HOLE(I)%PDY, HOLE(I)%PDZ
            write(2, '(2D17.9)') HOLE(I)%enc60, HOLE(I)%endop
            write(2, *) HOLE(I)%RW
            write(2, '(2D17.9)') HOLE(I)%dx, HOLE(I)%dt
            write(2, '(D16.8)') HOLE(I)%hop
        END DO

        DO I = 1,npartd - 2*ndop
            write(2, '(I2)') NEUT(I)%Zch
            write(2, '(12I8)') NEUT(I)%FN1
            write(2, '(I8)') NEUT(I)%site
            write(2, '(3F14.8)') NEUT(I)%PDX, NEUT(I)%PDY, NEUT(I)%PDZ
            write(2, '(2D17.9)') NEUT(I)%enc60, NEUT(I)%endop
            write(2, *) NEUT(I)%RW
            write(2, '(2D17.9)') NEUT(I)%dx, NEUT(I)%dt
            write(2, '(D16.8)') NEUT(I)%hop
        END DO


        CLOSE(UNIT=2)

END SUBROUTINE SNAPFLU

