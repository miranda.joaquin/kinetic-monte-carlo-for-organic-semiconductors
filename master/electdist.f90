SUBROUTINE ELECTDIST (step )
  USE DOUBLE
  USE arrays1, ONLY: HOLE, NEGA
  USE acommondata, ONLY: scal, ndop, cellc, szcell, tottime, DISTANCE
  USE RANDOM 
  INTEGER ho, el, step
  REAL (KIND=DP) :: RXIJ, RYIJ, RZIJ, RD!, avdist  
  REAL        :: N1, RX, RY, RZ 
  N1 = 0; RX = 0; RY = 0; RZ = 0
        
  OPEN(UNIT=25, FILE="stepmcdisel-ho.txt",  STATUS = 'OLD', ACTION = 'WRITE', POSITION='APPEND') 
!        OPEN(UNIT=26, FILE="stepmcdisel-el.txt",  STATUS = 'OLD', ACTION = 'WRITE', POSITION='APPEND') 
  DO el = 1, ndop
     DO ho = 1, ndop
        RXIJ =  NEGA(el)%PDX-HOLE(ho)%PDX
        RYIJ =  NEGA(el)%PDY-HOLE(ho)%PDY
        RZIJ =  NEGA(el)%PDZ-HOLE(ho)%PDZ
        RD   =  scal*DISTANCE(RXIJ, RYIJ, RZIJ)
        IF (RD .LE. 13.90) THEN
            RX   = RX + RXIJ
            RY   = RY + RYIJ
            RZ   = RZ + RZIJ 
            N1 = N1 + 1;
!        ELSE IF(RD .LT. 13.9d0 .and. RD .LE. 15.7) THEN
!             N2 = N2 + 1
!        ELSE IF(RD .LT. 15.7d0 .and. RD .LE. 25.7) THEN
!             N3 = N3 + 1
!        ELSE IF(RD .LT. 27.7d0 .and. RD .LE. 39.7) THEN
!             N4 = N4 + 1
        END IF
!                avdist = avdist + RD
     END DO
  END DO
       
  write(25,'(F17.8,2x,I6,1x,F14.6,1x,F14.6,1x,F14.6,1x,F14.6)') tottime,step,REAL(N1/ndop),&
                                                              scal*REAL(RX/ndop), scal*REAL(RY/ndop), scal*REAL(RZ/ndop) 
   !, REAL(szcell*cellc*avdist/(ndop))
  CLOSE(UNIT=25)

END SUBROUTINE
