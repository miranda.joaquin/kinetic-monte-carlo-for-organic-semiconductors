MODULE RGAUSS 
      
     USE DOUBLE 
! Non uniform random Number Generators in Fortran
!
      REAL (KIND=DP), PARAMETER :: PI=3.141592653589793238462
      CONTAINS

! Random Sample from normal (Gaussian) distribution
      
       FUNCTION rand_normal(mean,stdev) RESULT(c)
       DOUBLE PRECISION :: mean,stdev,c,temp(2),  theta, r
       IF(stdev <= 0.0d0) THEN

           WRITE(*,*) "Standard Deviation must be +ve"
       ELSE
          CALL RANDOM_NUMBER(temp)
          r=(-2.0d0*log(temp(1)))**0.5
          theta = 2.0d0*PI*temp(2)
          c= mean+stdev*r*sin(theta)
       END IF
      END FUNCTION

END MODULE RGAUSS
