MODULE CLASS_FLUORS
    USE DOUBLE
    IMPLICIT NONE

    TYPE, PUBLIC :: fluors

!    SEQUENCE
    INTEGER        :: Zch           !Charge 
    INTEGER        :: FN1 (1:12)    !nearest nn level
!    INTEGER        :: FN2 (1:6)
    INTEGER        :: site          !Position in the flu list mapped matrix
    REAL (KIND=DP) :: PDX, PDY, PDZ !PP Position in the matrix
    REAL (KIND=DP) :: enc60, endop  !Energy with all other C60s and with all dopants
    REAL (KIND=DP) :: RW (1:12)
    REAL (KIND=DP) :: dx, dt
    INTEGER        :: hop  
    END TYPE fluors 

END MODULE CLASS_FLUORS
