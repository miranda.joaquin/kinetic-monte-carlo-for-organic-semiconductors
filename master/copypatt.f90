SUBROUTINE COPYPATT ()
        USE DOUBLE
        USE POTENTIALS
        USE RGAUSS
        USE arrays1, ONLY:  HOLE, NEGA, flu, res 
        USE RANDOM
        USE acommondata, ONLY: ndop, cellc, convl, szcell, DISTANCE  
        IMPLICIT NONE
        INTEGER I, J, K, p,  N, randn1, sumper, testnn, testdop, nene
        REAL (kIND=DP) :: randpos, mean, stdv
        REAL (KIND=DP) :: lx, ly, lz
        testdop = 0; N=0
        mean = 0; stdv = 0.00   !Values for Gaussian function 
        nene = 12               !Number of nearest neighbours
        p = 0; J = 0; K = 0; testnn = -1
       
        PRINT*, "distortion around dopant", stdv

        CALL RANGENdop()
        DO WHILE (testdop .LT. ndop) 
           sumper = 1
           p = p + 1
           IF (-1 .NE. flu(res(p))%Zch) THEN
               DO I = 1, 12 
!                  print*, res(p), flu(res(p))%FN1(I) 
                  sumper = sumper*flu(flu(res(p))%FN1(I))%Zch
               END DO
               IF ((sumper .EQ. 0)) THEN
!                    WRITE(*,*) res(p), "p doped GO!"
                     flu(res(p))%Zch = 1
                     J = J + 1
                     HOLE(J) = flu(res(p))
                     DO WHILE(testnn .LT. 0 )
                        CALL RANDOM_NUMBER(randpos)
                        randn1=12*randpos+1
                        IF(0 .EQ. ( flu(flu(res(p))%FN1(randn1))%Zch )) THEN
                            testnn = 1
                            flu(flu(res(p))%FN1(randn1))%Zch = -1!random for the N1 neighbours in the shell
                            NEGA(J)  =  flu(flu(res(p))%FN1(randn1))
                        END IF 
                     END DO
                     DO I =1, 12
                        flu(flu(res(p))%FN1(I))%PDX = flu(flu(res(p))%FN1(I))%PDX + rand_normal(mean,stdv)/REAL(cellc) !XC60(I)
                        flu(flu(res(p))%FN1(I))%PDY = flu(flu(res(p))%FN1(I))%PDY + rand_normal(mean,stdv)/REAL(cellc) !YC60(I)
                        flu(flu(res(p))%FN1(I))%PDZ = flu(flu(res(p))%FN1(I))%PDZ + rand_normal(mean,stdv)/REAL(cellc) !ZC60(I)
                        lx = flu(flu(res(p))%FN1(I))%PDX - ANINT(flu(flu(res(p))%FN1(I))%PDX)
                        ly = flu(flu(res(p))%FN1(I))%PDY - ANINT(flu(flu(res(p))%FN1(I))%PDY)
                        lz = flu(flu(res(p))%FN1(I))%PDZ - ANINT(flu(flu(res(p))%FN1(I))%PDZ)
                        flu(flu(res(p))%FN1(I))%PDX = lx
                        flu(flu(res(p))%FN1(I))%PDY = ly
                        flu(flu(res(p))%FN1(I))%PDZ = lz
!                        lx = flu(flu(res(p))%FN1(I))%PDX-flu(res(p))%PDX
!                        ly = flu(flu(res(p))%FN1(I))%PDY-flu(res(p))%PDY
!                        lz = flu(flu(res(p))%FN1(I))%PDZ-flu(res(p))%PDZ
!                        print*,"F  ", szcell*cellc*DISTANCE(lx,ly,lz) 
                     END DO
 
                     testdop = testdop + 1
               END IF
               testnn = -1
           END IF
        END DO

!        WRITE(*,*) "DONE COPY and electrons released by ndopes !", testdop, N 

END SUBROUTINE COPYPATT
