subroutine SEARCH (shift )

  USE acommondata, ONLY: ndop
  USE arrays1, ONLY: NEGA, freqs
  implicit none
  real(16)  :: rho, testratio
  integer ::  k, p, kk, pp, neighn, c60, shift

  LOOP1: DO p = 1,  ndop + shift !Search for first non-zero RW
     DO k = 1, 12
        IF ( 0.d0 .LT. NEGA(p)%RW(k) ) THEN
            call random_number ( rho )
            testratio = -1/NEGA(p)%RW(k)*log(rho)
            freqs%ncharge  = p
            freqs%hopnn    = k
            freqs%nnratio  = testratio
            EXIT LOOP1
        END IF
     END DO
   END DO LOOP1

!   IF (12 == k .and. p .ne. ndop) THEN ! Adjust k and p after hard EXITING LOOP
!        k = 1
!        p = p + 1
!   ELSE IF (12 == k .and. p == ndop) THEN
!            return
!   END IF


   DO pp = p,  ndop + shift ! Keep searching for rest of charges hopping to neaest neighbours
      DO kk = k, 12
         IF ( 0.d0 .LT. NEGA(pp)%RW(kk) ) THEN
             call random_number ( rho )
             IF (testratio .GT. -1/NEGA(pp)%RW(kk)*log(rho)) THEN
                testratio = -1/NEGA(pp)%RW(kk)*log(rho) !Exchange for the smallest
                freqs%ncharge  = pp
                freqs%hopnn    = kk
                freqs%nnratio  = testratio   
             END IF
         END IF
      END DO
   END DO

!   IF(0==neighn .or. 12 .LT. neighn) print*, 's', freqs%hopnn, k, p, kk, pp
!   freqs%ncharge  = c60
!   freqs%hopnn    = neighn
!   freqs%nnratio  = testratio

  return
end

