SUBROUTINE memarrays
      
        Use acommondata, ONLY: npartd, ndop, listtry 
        USE arrays1
        IMPLICIT NONE
        INTEGER :: AllocateStatus

!        ALLOCATE(IRAND(1:listtry), STAT = AllocateStatus)
!        IF (AllocateStatus /= 0) STOP "***Not enough memory***"
        ALLOCATE(ITRY(1:listtry), STAT = AllocateStatus)
        IF (AllocateStatus /= 0) STOP "***Not enough memory***"
        ALLOCATE( flu(1:npartd), STAT = AllocateStatus)
        IF (AllocateStatus /= 0) STOP "***Not enough memory***"
        ALLOCATE( res(1:listtry), STAT = AllocateStatus)
        IF (AllocateStatus /= 0) STOP "***Not enough memory***"

        ALLOCATE(HOLE(1:ndop + 1 ), STAT = AllocateStatus)
        IF (AllocateStatus /= 0) STOP "***Not enough memory***"
        ALLOCATE(NEGA(1:ndop + 1), STAT = AllocateStatus)
        IF (AllocateStatus /= 0) STOP "***Not enough memory***"
        ALLOCATE(NEUT(1:npartd - 2*ndop), STAT = AllocateStatus)
        IF (AllocateStatus /= 0) STOP "***Not enough memory***"
!        ALLOCATE(fixNEGA(1:ndop + 1), STAT = AllocateStatus)
!        IF (AllocateStatus /= 0) STOP "***Not enough memory***"

!        ALLOCATE(freqs(1:12*ndop), STAT = AllocateStatus)
!        IF (AllocateStatus /= 0) STOP "***  ***"

END SUBROUTINE memarrays
            
SUBROUTINE freememarrays

        USE arrays1
        IMPLICIT NONE
        INTEGER :: DeAllocateStatus
      
        DEALLOCATE( HOLE, STAT = DeAllocateStatus)
        IF (DeAllocateStatus /= 0) STOP "***  ***"
        DEALLOCATE( NEGA, STAT = DeAllocateStatus)
        IF (DeAllocateStatus /= 0) STOP "***  ***"
!        DEALLOCATE( fixNEGA, STAT = DeAllocateStatus)
!        IF (DeAllocateStatus /= 0) STOP "***  ***"
        DEALLOCATE( flu, STAT = DeAllocateStatus)
        IF (DeAllocateStatus /= 0) STOP "***  ***"

!        DEALLOCATE( freqs, STAT = DeAllocateStatus)
!        IF (DeAllocateStatus /= 0) STOP "***  ***"

END SUBROUTINE
 
