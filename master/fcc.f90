        SUBROUTINE FCC()

!  *******************************************************************
!  ** SETS UP THE ALPHA FCC LATTICE FOR N LINEAR MOLECULES.         **
!  **                                                               **
!  ** THE SIMULATION BOX IS A UNIT CUBE CENTRED AT THE ORIGIN.      **
!  ** N SHOULD BE AN INTEGER OF THE FORM ( 4 * ( NC ** 3 ) ),       **
!  ** WHERE NC IS THE NUMBER OF FCC UNIT CELLS IN EACH DIRECTION.   **
!  ** SEE FIGURE 5.10 FOR A DIAGRAM OF THE LATTICE AND A            **
!  ** DEFINITION OF THE FOUR ORIENTATIONAL SUBLATTICES.             **
!  **                                                               **
!  ** PRINCIPAL VARIABLES:                                          **
!  **                                                               **
!  ** INTEGER N                    NUMBER OF MOLECULES              **
!  ** REAL    RX(N),RY(N),RZ(N)    MOLECULAR POSITIONS              **
!  ** REAL    EX(N),EY(N),EZ(N)    UNIT VECTORS GIVING ORIENTATIONS **
!  ** REAL    RROOT3               1.0 / SQRT ( 3.0 )               **
!  *******************************************************************
        USE DOUBLE
        USE RGAUSS
        USE arrays1, ONLY: flu 
        USE acommondata, ONLY: npartd, DISTANCE, DF, cellc
        IMPLICIT NONE 
        REAL (KIND=DP) :: RXIJ, RYIJ, RZIJ, RD
        INTEGER        :: N, NC, N1, N2
        REAL (KIND=DP) :: RROOT3
!        PARAMETER ( NC = 3, N = 4 * NC ** 3 )
        PARAMETER ( RROOT3 = 0.5773503 )
        REAL (KIND=DP) :: CELL, CELL2
        REAL (KIND=DP) :: mean, stdv
        INTEGER        :: I, J, IX, IY, IZ, IREF, M
        REAL (KIND=DP), ALLOCATABLE :: RX(:), RY(:), RZ(:)
        INTEGER :: AllocateStatus
       
        NC = cellc
        N  = npartd    !4 * NC ** 3
        DF(1) = 0.70710678118654757/REAL(NC) !Nearest Neighbor distance in FCC
        DF(2) = 1.d0/REAL(NC)
        mean = 0; stdv = 0.0           !Values for Gaussian function
        PRINT*, "stdv for istortion in all fcc sites", stdv

        ALLOCATE( RX(1:npartd), STAT = AllocateStatus)
        IF (AllocateStatus /= 0) STOP "***Not enough memory ***"
        ALLOCATE( RY(1:npartd), STAT = AllocateStatus)
        IF (AllocateStatus /= 0) STOP "***Not enough memory***"
        ALLOCATE( RZ(1:npartd), STAT = AllocateStatus)
        IF (AllocateStatus /= 0) STOP "***Not enough memory***"

       
!  *******************************************************************

!  ** CALCULATE THE SIDE OF THE UNIT CELL **

        CELL  = 1.0 / REAL ( NC )
        CELL2 = 0.5 * CELL

!  ** BUILD THE UNIT CELL **

!  ** SUBLATTICE A **

        RX(1) =  0.0
        RY(1) =  0.0
        RZ(1) =  0.0
!
!  ** SUBLATTICE B **

        RX(2) =  CELL2
        RY(2) =  CELL2
        RZ(2) =  0.0

!  ** SUBLATTICE C **

        RX(3) =  0.0
        RY(3) =  CELL2
        RZ(3) =  CELL2

!  ** SUBLATTICE D **

        RX(4) =  CELL2
        RY(4) =  0.0
        RZ(4) =  CELL2

!  ** CONSTRUCT THE LATTICE FROM THE UNIT CELL **

        M = 0
        DO  IZ = 1, NC
           DO  IY = 1, NC
              DO  IX = 1, NC
                 DO  IREF = 1, 4
                    RX(IREF+M) = RX(IREF) + CELL * REAL ( IX - 1 )
                    RY(IREF+M) = RY(IREF) + CELL * REAL ( IY - 1 )
                    RZ(IREF+M) = RZ(IREF) + CELL * REAL ( IZ - 1 )
                 ENDDO 
                 M = M + 4
              ENDDO 
           ENDDO 
        ENDDO 

!  ** SHIFT CENTRE OF BOX TO THE ORIGIN **

       M = 0 
       DO  I = 1, N
           RX(I) = RX(I) - 0.5
           RY(I) = RY(I) - 0.5
           RZ(I) = RZ(I) - 0.5
           flu(I)%Zch  = 0
           flu(I)%PDX  = RX(I)
           flu(I)%PDY  = RY(I)
           flu(I)%PDZ  = RZ(I)
!!           flu(I)%site = I
!           PRINT*, "H", RX(I), RY(I), RZ(I)
       ENDDO 

       !Find nearest neighbours
       N1 = 0
!       N2 = 0
       DO I = 1, N 
            DO J = 1, N
            flu(I)%site    = I
               IF(I .NE. J) THEN
                    RXIJ = flu(I)%PDX-flu(J)%PDX
                    RYIJ = flu(I)%PDY-flu(J)%PDY
                    RZIJ = flu(I)%PDZ-flu(J)%PDZ
                    RD   = DISTANCE(RXIJ, RYIJ, RZIJ);
!                    WRITE(*,*)  "DISTANCES", RD, RXIJ, RYIJ, RZIJ 
                    IF (abs(RD - DF(1)) .LE. 0.0000001) THEN
!                         WRITE(*,*)  "DISTANCES", RD, RXIJ, RYIJ, RZIJ 
                          N1             = N1 + 1;
                          flu(I)%FN1(N1) = J
!                    ELSE IF (abs(RD - DF(2)) .LE. 0.0000001) THEN
!                         WRITE(*,*)  "DISTANCES", RD, RXIJ, RYIJ, RZIJ 
!                          N2             = N2 + 1;
!                          flu(I)%FN2(N2) = J
                    END IF
                END IF
            END DO
            N1 = 0
!            N2 = 0
       END DO

!      DO I = 1, N
!          print flu(I)%FN1
!          print flu(I)%FN2
!      END DO

       !Add randomness in sites
       DO I = 1, N
             flu(I)%PDX = flu(I)%PDX + rand_normal(mean,stdv)/REAL ( NC )
             flu(I)%PDY = flu(I)%PDY + rand_normal(mean,stdv)/REAL ( NC )
             flu(I)%PDZ = flu(I)%PDZ + rand_normal(mean,stdv)/REAL ( NC )
       END DO

        DEALLOCATE( RX, STAT = AllocateStatus)
        IF (AllocateStatus /= 0) STOP "***  ***"
        DEALLOCATE( RY, STAT = AllocateStatus)
        IF (AllocateStatus /= 0) STOP "***  ***"
        DEALLOCATE( RZ, STAT = AllocateStatus)
        IF (AllocateStatus /= 0) STOP "***  ***"
      
END SUBROUTINE FCC


