PROGRAM MCC60
   USE DOUBLE
   USE acommondata
   USE arrays1
   USE RANDOM
   USE POTENTIALS, ONLY: FIXPOS
   IMPLICIT NONE
   INTERFACE
       SUBROUTINE memarrays
   END SUBROUTINE memarrays
   END INTERFACE
   INTERFACE
       SUBROUTINE freememarrays
   END SUBROUTINE freememarrays
   END INTERFACE
   INTEGER         :: I, J, step, nexmc, AllocateStatus
!   TYPE (fluors)   :: tempNEGA
   CHARACTER(len=1024) :: filename
   tottime  = 0.d0
   totdx    = 0.d0 
   nexmc    = 999999
   npartd   = 4*cellc*cellc*cellc !FCC number of molecules in cell

   CALL memarrays
   CALL setrn(iseed)
   CALL FCC()                   !Write template of a FCC crystal on flus
   CALL COPYPATT()              !Create dopants and associated electrons
   DEALLOCATE( res, STAT = AllocateStatus)
   IF (AllocateStatus /= 0) STOP "***  ***"
   DEALLOCATE( ITRY, STAT = AllocateStatus)
   CALL GETLIST2()              !Write the flus on NEGA, DOPA, NEUT arrays
   CALL FIXPOS(VR)              !Energy only from all pairs dopant-dopant 

   PRINT*, " tempera  ndop  npartd  nmc  nexmc  ldop  efield"
   PRINT*,   tempera, ndop, npartd, nmc, nexmc, ldop, efield
   PRINT*, "VR",  VR
   OPEN(UNIT=22, FILE="stepmcen.txt",  STATUS = 'OLD', ACTION = 'WRITE',POSITION='APPEND')
   OPEN(UNIT=24, FILE="1stepmcen.txt",  STATUS = 'OLD', ACTION = 'WRITE', POSITION='APPEND')
   OPEN(UNIT=23, FILE="dx.txt",  ACTION = 'WRITE', POSITION='APPEND')

   CALL TOTENERGY(0)
   DO I = 1, nmc                   !Thermalize 
       CALL MOVELSW()              !Decide if move charge,swap positions,or stay
!       CALL TOTENERGY(I) !Calculate energy and write it in stepmcen.txt
!       CALL ELECTDIST (I) 
       IF (mod(I, 10) .LE. 0.000001) THEN
           CALL TOTENERGY(I)
           CALL ELECTDIST(I)
!          K = K +1 
!          CALL SNAP(K)
!          CALL SNAPALL(K)
       END IF
   END DO

   CALL GETLIST3()                 !Be sure flu has the latest configuration taken from NEGA and NEUT

   DO I = 1, 12
      flu%RW(I) = 0.d0
   END DO
   flu%enc60  = 0.d0
   flu%endop  = 0.d0
   NEGA%endop = 0.d0
   NEGA%enc60 = 0.d0
   flu%dx     = 0.d0
   flu%dt     = 0.d0
   NEGA%dx    = 0.d0
   NEGA%dt    = 0.d0
   NEGA%hop   = 0

   CALL RATIOS0()
   CALL DOSAD(0)
   CALL DOSRE(0)
   CALL DOSNE(0)
   CALL SNAPALL(0)
   CALL SNAPFLU(0)
   DO  J = 1, nexmc
       CALL KMC(J)
       WRITE(23, *),  tottime/unitevfreq, totdx
 !      CALL SNAPALL(J)                                          !If allow it, it will create create several observable 
!       IF (mod(J, 3250) .LE. 0.000001)  THEN !MAke histograms   !which are save for each pass fullfilling the J/N condition
!            CALL ELECTDIST(J)					 !hence also slowing down the collection of distance displaced
!            CALL GETLISTN()
!            CALL DOSAD(J/650)
!            CALL DOSRE(J/650)
!            CALL DOSNE(J/650)
!            CALL SNAPALL(J/650) 
!             CALL SNAPFLU(J/3250)
!            WRITE (filename, "(I5,A4, f3.2, A1, I2, A4)") INT(J/650),
!            "conf",ldop,"l", cellc, ".txt"
!            OPEN (UNIT=21, FILE=filename, STATUS='NEW', ACTION='WRITE')
!            DO I = 1, ndop 
!               WRITE(21, *)  I, NEGA(I)%hop, scal*NEGA(I)%dx, NEGA(I)%dt/s2phbr
!            END DO
!            CLOSE(UNIT=21)
!       END IF
   END DO

   CALL GETLISTN()
   CALL SNAPFLU(1)
   CALL SNAPALL(1)
   CALL DOSAD(1)
   CALL DOSRE(1)
   CALL DOSNE(1)

   CLOSE(UNIT=22)
   CLOSE(UNIT=23)
   CLOSE(UNIT=24)
   CALL freememarrays
   DEALLOCATE( NEUT, STAT = AllocateStatus)
   IF (AllocateStatus /= 0) STOP "***  ***"

   CLOSE(UNIT=22)
   CLOSE(UNIT=23)
   CLOSE(UNIT=24) 
   CALL freememarrays
   DEALLOCATE( NEUT, STAT = AllocateStatus)
   IF (AllocateStatus /= 0) STOP "***  ***"

END PROGRAM  MCC60 

FUNCTION  MILLER(dist, dx, deltE)
   USE DOUBLE
   USE acommondata, ONLY: evtemp, invtemp
   IMPLICIT NONE
   REAL (KIND=DP) :: dist, dx, deltE, efield, MILLER

   MILLER = 500*exp(-dist)
   IF (deltE .LT. 0.d0) THEN
      MILLER = MILLER*exp(invtemp*deltE)
   END IF 
END FUNCTION  MILLER

FUNCTION  MARCUS(dist, dx, deltE)
   USE DOUBLE
   USE acommondata, ONLY:  efield, LkT 
   IMPLICIT NONE
   REAL (KIND=DP) :: dist, dx, deltE, arg, MARCUS
!   s2phbr =sqrt(2*3.1415926535897932384626)/(6.5821*1e-16*1e15) !3.80823877364 
!   kT     = 0.0259 eV at T =300 K      
!   alfa   = 1.d0   !(alfa = a/10A)
!   en60   = 0.025d0 !eV
!   Lambda = 0.076d0 !eV
   arg    = deltE - 0.076d0 - efield*dx
!   MARCUS = s2phbr*sqrt(1/LkT)*0.000625d0*exp(-2.d0*(dist-10))*exp(-arg*arg/LkT/4)
   MARCUS = exp(-2.d0*(dist-10))*exp(-arg*arg/LkT/4) 
END FUNCTION  MARCUS

SUBROUTINE KMC(step)
   USE DOUBLE
   USE acommondata, ONLY: ndop, tottime, totdx, efield, cellc, DISTANCE, invtemp
   USE arrays1, ONLY: NEGA, flu, freqs
   USE RANDOM
   USE POTENTIALS, ONLY: C60L, C60M, COULOMB, DOPELECT_POT
   USE CLASS_FLUORS
   IMPLICIT NONE
   REAL(KIND=DP)  :: Vf1R, Vf2R, XIJ, tmpdx, tmpdt
   INTEGER        :: step, arriv, arriv2, leave, tmphop

   CALL SEARCH (0)       !Sort times
   leave = freqs%ncharge
   arriv = freqs%hopnn
   XIJ   = NEGA(leave)%PDX
!   IF (arriv .LT. 13) THEN
        XIJ = XIJ - flu(NEGA(leave)%FN1(arriv))%PDX
!   ELSE
!        arriv2 = 19 - arriv
!        print*, arriv, arriv2, '2nd neigh'
!        XIJ = XIJ - flu(NEGA(leave)%FN2(arriv2))%PDX
!   END IF
   XIJ  = XIJ - ANINT ( XIJ )
   CALL C60L(leave)   !Remove in all C60s the energy contribution from the chosen C60(I)

   tmpdx  = NEGA(leave)%dx
   tmpdt  = NEGA(leave)%dt
   tmphop = NEGA(leave)%hop

!  Move from the chosen charge to new available site       
   flu(NEGA(leave)%site)%Zch       =  0                          !Leave site once choosen to hop by kMC
!   IF (arriv .LT. 13) THEN
        flu(NEGA(leave)%FN1(arriv))%Zch = -1                     !Arrive next nearest neigh site
        NEGA(leave)                = flu(NEGA(leave)%FN1(arriv)) !New NEGA list updated
!   ELSE
!        flu(NEGA(leave)%FN2(arriv2))%Zch = -1                    !Arrive second nearest neigh site 
!        NEGA(leave)                = flu(NEGA(leave)%FN2(arriv2)) !New NEGA list updated
!   END IF

   CALL COULOMB(leave, Vf1R)                                     !New energy values of NEGA(p)
   CALL DOPELECT_POT(leave, Vf2R)
   NEGA(leave)%enc60 = Vf1R
   NEGA(leave)%endop = Vf2R
   CALL C60M(leave)                    !Add in all C60s the energy contribution from new C60(I)
   NEGA(leave)%dx  = tmpdx
   NEGA(leave)%dt  = tmpdt 
   NEGA(leave)%hop = tmphop
   NEGA(leave)%dx  = NEGA(leave)%dx + XIJ
   NEGA(leave)%dt  = NEGA(leave)%dt + freqs%nnratio
   NEGA(leave)%hop = NEGA(leave)%hop + 1
   tottime = tottime + freqs%nnratio
   totdx   = totdx + XIJ
   CALL RATIOS2(leave)
!   CALL TOTENERGY2(step)
END SUBROUTINE KMC

SUBROUTINE RATIOS0()
  USE DOUBLE
  USE acommondata, ONLY: ndop, fact, invtemp, scal
  USE CLASS_FLUORS
  USE POTENTIALS, ONLY: COULOMB, DOPELECT_POT
  USE arrays1, ONLY: flu, NEGA
  IMPLICIT NONE
  INTEGER :: p, I
  REAL (KIND=DP) :: enp, Vf1R, Vf2R, MILLER, MARCUS, enp1
  REAL (KIND=DP) :: RXIJ, RYIJ, RZIJ, rx, ry, rz, dist
  TYPE (fluors)  :: tempNEGA

  DO p = 1, ndop
      CALL COULOMB(p, Vf1R)     !Energy between pair of electrons
      CALL DOPELECT_POT(p, Vf2R)!Energy between electron and all dopes by defined potential
      NEGA(p)%enc60         = Vf1R
      NEGA(p)%endop         = Vf2R
      enp1                  = Vf1R + Vf2R
      tempNEGA              = NEGA(p)
      flu(NEGA(p)%site)%Zch = 0
      rx = tempNEGA%PDX
      ry = tempNEGA%PDY
      rz = tempNEGA%PDZ

      DO I = 1, 12
         IF( 0 .EQ.  flu(tempNEGA%FN1(I))%Zch ) THEN
             flu(tempNEGA%FN1(I))%Zch = -1
             NEGA(p)                  = flu(tempNEGA%FN1(I))
             CALL COULOMB(p, Vf1R)
             CALL DOPELECT_POT(p, Vf2R)
             enp  = enp1 - Vf1R - Vf2R
             RXIJ = rx - NEGA(p)%PDX
             RYIJ = ry - NEGA(p)%PDY
             RZIJ = rz - NEGA(p)%PDZ
             RXIJ = RXIJ - ANINT ( RXIJ )
             RYIJ = RYIJ - ANINT ( RYIJ )
             RZIJ = RZIJ - ANINT ( RZIJ )
             dist = scal*SQRT ( RXIJ * RXIJ + RYIJ * RYIJ + RZIJ * RZIJ )
             tempNEGA%RW(I) = MARCUS(dist, scal*RXIJ, enp)
             flu(tempNEGA%FN1(I))%Zch = 0
         ELSE
             tempNEGA%RW(I) = 0.d0
         END IF
      END DO

!      DO I = 1, 6	!Allow to hop to 2NN
!         IF( 0 .EQ.  flu(tempNEGA%FN2(I))%Zch ) THEN
!             flu(tempNEGA%FN2(I))%Zch = -1
!             NEGA(p)                  = flu(tempNEGA%FN2(I))
!             CALL COULOMB(p, Vf1R)
!             CALL DOPELECT_POT(p, Vf2R)
!             enp  = enp1 - Vf1R - Vf2R
!             RXIJ = rx - NEGA(p)%PDX
!             RYIJ = ry - NEGA(p)%PDY
!             RZIJ = rz - NEGA(p)%PDZ
!             RXIJ = RXIJ - ANINT ( RXIJ )
!             RYIJ = RYIJ - ANINT ( RYIJ )
!             RZIJ = RZIJ - ANINT ( RZIJ )
!             dist = scal*SQRT ( RXIJ * RXIJ + RYIJ * RYIJ + RZIJ * RZIJ )
!             tempNEGA%RW(12 + I) = MARCUS(dist, scal*RXIJ, enp)
!             flu(tempNEGA%FN2(I))%Zch = 0
!         ELSE
!             tempNEGA%RW(12 + I) = 0.d0
!         END IF
!      END DO

      NEGA(p)               = tempNEGA
      flu(NEGA(p)%site)%Zch = -1
  END DO

END SUBROUTINE RATIOS0

SUBROUTINE RATIOS2(source)
  USE DOUBLE
  USE acommondata, ONLY:  ndop, invtemp, scal 
  USE CLASS_FLUORS
  USE POTENTIALS, ONLY: COULOMB, DOPELECT_POT
  USE arrays1, ONLY: flu, NEGA
  IMPLICIT NONE
  INTEGER :: p, I, source
  REAL (KIND=DP) :: enp, Vf1R, Vf2R, MILLER, MARCUS, enp1
  REAL (KIND=DP) :: RXIJ, RYIJ, RZIJ, rx, ry, rz, dist
  TYPE (fluors)  :: tempNEGA

  CALL DOPELECT_POT(source, Vf2R)    !New electron-hole energy for new position 
  NEGA(source)%endop          = Vf2R !of the moved, source, charge

  DO p = 1, ndop
       enp1                   = NEGA(p)%enc60 + NEGA(p)%endop
       tempNEGA               = NEGA(p)
       flu(NEGA(p)%site)%Zch  = 0
       rx = tempNEGA%PDX
       ry = tempNEGA%PDY
       rz = tempNEGA%PDZ
       flu(NEGA(p)%site)%Zch = 0

     DO I = 1, 12
         IF( 0 .EQ.  flu(tempNEGA%FN1(I))%Zch ) THEN
             flu(tempNEGA%FN1(I))%Zch = -1
             NEGA(p)                  = flu(tempNEGA%FN1(I))
             CALL COULOMB(p, Vf1R)
             CALL DOPELECT_POT(p, Vf2R)
             enp  = enp1 - Vf1R - Vf2R
             RXIJ = rx - NEGA(p)%PDX
             RYIJ = ry - NEGA(p)%PDY
             RZIJ = rz - NEGA(p)%PDZ
             RXIJ = RXIJ - ANINT ( RXIJ )
             RYIJ = RYIJ - ANINT ( RYIJ )
             RZIJ = RZIJ - ANINT ( RZIJ )
             dist = scal*SQRT ( RXIJ * RXIJ + RYIJ * RYIJ + RZIJ * RZIJ )
             tempNEGA%RW(I) = MARCUS(dist, scal*RXIJ, enp)
             flu(tempNEGA%FN1(I))%Zch = 0
         ELSE
             tempNEGA%RW(I) = 0.d0
         END IF
      END DO

!      DO I = 1, 6
!         IF( 0 .EQ.  flu(tempNEGA%FN2(I))%Zch ) THEN
!             flu(tempNEGA%FN2(I))%Zch = -1
!             NEGA(p)                  = flu(tempNEGA%FN2(I))
!             CALL COULOMB(p, Vf1R)
!             CALL DOPELECT_POT(p, Vf2R)
!             enp  = enp1 - Vf1R - Vf2R
!             RXIJ = rx - NEGA(p)%PDX
!             RYIJ = ry - NEGA(p)%PDY
!             RZIJ = rz - NEGA(p)%PDZ
!             RXIJ = RXIJ - ANINT ( RXIJ )
!             RYIJ = RYIJ - ANINT ( RYIJ )
!             RZIJ = RZIJ - ANINT ( RZIJ )
!             dist = scal*SQRT ( RXIJ * RXIJ + RYIJ * RYIJ + RZIJ * RZIJ )
!             tempNEGA%RW(12 + I) = MARCUS(dist, scal*RXIJ, enp)
!             flu(tempNEGA%FN2(I))%Zch = 0
!         ELSE
!             tempNEGA%RW(12 + I) = 0.d0
!         END IF
!      END DO

      NEGA(p)               = tempNEGA
      flu(NEGA(p)%site)%Zch = -1
 
  END DO

END SUBROUTINE RATIOS2

SUBROUTINE GETLIST2()
  USE DOUBLE
  USE acommondata, ONLY: npartd
  USE arrays1, ONLY: flu, NEGA, HOLE, NEUT
  IMPLICIT NONE
  INTEGER :: I, oc, un, po
  oc = 0; un = 0; po = 0

  DO I = 1, npartd
      IF (-1 .EQ. flu(I)%Zch) THEN
         oc = oc + 1
         NEGA(oc) = flu(I)
      ELSE IF (1 .EQ. flu(I)%Zch) THEN
         po = po + 1
         HOLE(po) = flu(I)
      ELSE
         un = un + 1
         NEUT(un) = flu(I)
      END IF
  END DO
       
ENDSUBROUTINE GETLIST2

SUBROUTINE GETLIST3()
  USE DOUBLE
  USE acommondata
  USE arrays1, ONLY: flu, NEGA, HOLE
  IMPLICIT NONE
  INTEGER :: I 

  flu%Zch = 0
  DO I = 1, ndop
      flu(NEGA(I)%site)%Zch = NEGA(I)%Zch
      flu(HOLE(I)%site)%Zch = HOLE(I)%Zch
  END DO
       
ENDSUBROUTINE GETLIST3

SUBROUTINE GETLISTN()
  USE DOUBLE
  USE acommondata, ONLY: npartd
  USE arrays1, ONLY: flu, NEUT
  IMPLICIT NONE
  INTEGER :: I, un
  un = 0

  DO I = 1, npartd
      IF (0 .EQ. flu(I)%Zch) THEN
         un = un + 1
         NEUT(un) = flu(I)
      END IF
  END DO

ENDSUBROUTINE GETLISTN

SUBROUTINE RANGEN(diro)
  USE DOUBLE
  USE RANDOM
  IMPLICIT NONE
  REAL (KIND=DP) u(3), diro(1:3)
  CALL RANDOM_NUMBER(u)
  diro = -1+floor( 3*u )
END SUBROUTINE RANGEN

SUBROUTINE remove_dups(example, k)
  USE acommondata, ONLY: listtry
  USE arrays1, ONLY: res
  IMPLICIT NONE
  integer :: example(1:listtry)   !The input
!  integer :: res(size(example))  !The output
  integer :: k                    !The number of unique elements
  integer :: i, j

!  example = [1, 2, 3, 2, 2, 4, 5, 5, 4, 6, 6, 5]
  k = 1
  res(1) = example(1)
  outer: do i=2,size(example)
     do j=1,k
        if (res(j) == example(i)) then
           ! Found a match so start looking again
           cycle outer
        end if
     end do
     ! No match found so add it to the output
     k = k + 1
     res(k) = example(i)
  end do outer
!  write(*,advance='no',fmt='(a,i0,a)') 'Unique list has ',k,' elements: '
end SUBROUTINE remove_dups

SUBROUTINE RANGENdop()
  USE DOUBLE
  USE arrays1, ONLY: ITRY, res
  USE acommondata, ONLY:  listtry, npartd
  IMPLICIT NONE
  REAL*4  :: uu(1:listtry)
  INTEGER  iiseed/2817/
  INTEGER :: k !, remove_dups
  res = 0
  call random_number(uu)
  ITRY = int(uu*npartd)+1 !Following formula for interval lim1,lim2: int(rand(0)*(lim2+1-lim1))+lim1
!  ITRY = floor((npartd+1)*uu)
  CALL remove_dups(ITRY, k)
END SUBROUTINE RANGENdop

SUBROUTINE TOTENERGY(mcstep)
  USE DOUBLE
  USE acommondata, ONLY: ndop, tottime !VR
  USE POTENTIALS, ONLY: COULOMBALL, DOPELECT_POT
  USE OMP_LIB
  IMPLICIT NONE
  INTEGER        :: p, mcstep
  REAL (KIND=DP) :: totEewf1, Vf1R, Vf2R, totpot
  totEewf1 = 0.0
  totpot   = 0.0

  CALL COULOMBALL(Vf1R)    !Energy between pair of electrons

!$OMP PARALLEL SHARED(ndop) PRIVATE(p, Vf2R) REDUCTION (+: totpot) 
!$OMP DO SCHEDULE(AUTO)
  DO p = 1, ndop
     CALL DOPELECT_POT(p, Vf2R) !Energy between electron and all dopes bydefined 
     totpot = totpot + Vf2R                                  !potential, except NN
  END DO
!$OMP END DO
!$OMP END PARALLEL

  totEewf1 = Vf1R + totpot
  write(22, '(F14.6,1x,I5,1x,F14.6,1x,F14.6,1x,F14.6)') tottime, mcstep, totEewf1, Vf1R, totpot

END SUBROUTINE TOTENERGY

SUBROUTINE TOTENERGY2(mcstep)
  USE DOUBLE
  USE acommondata, ONLY: ndop, tottime !, VR
  USE arrays1, ONLY: NEGA
  USE OMP_LIB
  IMPLICIT NONE 
  INTEGER        :: p, mcstep
  REAL (KIND=DP) :: totpot, totpot2
  totpot = 0.d0; totpot2 = 0.d0

!!$OMP PARALLEL SHARED(ndop, NEGA) PRIVATE(p,  totpot2, totpot) REDUCTION (+: totpot, totpot2) 
!!$OMP DO SCHEDULE(AUTO)
  DO p = 1, ndop
     totpot  = totpot  + NEGA(p)%enc60
     totpot2 = totpot2 + NEGA(p)%endop                                  
  END DO
!!$OMP END DO
!!$OMP END PARALLEL

  totpot = totpot/2
  write(24, '(F16.6,1x,I5,1x,F14.6,1x,F14.6,1x,F14.6)') tottime, mcstep, totpot + totpot2, totpot, totpot2 

END SUBROUTINE TOTENERGY2

SUBROUTINE DOSNE(step)
  USE DOUBLE
  USE acommondata, ONLY: ndop, ldop, cellc, scal 
  USE arrays1, ONLY: NEGA
  USE OMP_LIB
  IMPLICIT NONE
  INTEGER             :: J, step
  CHARACTER(len=1024) :: filename

  WRITE (filename, "(I5,A4, f3.2, A1, I2, A4)") step, "dos0",ldop,"l",cellc, ".dat"
  OPEN (UNIT=21, FILE=filename, STATUS='NEW', ACTION='WRITE')

  DO J = 1, ndop
    WRITE(21,'(F14.8,1x,F12.6,1x,F12.6,1x,F12.6)') NEGA(J)%enc60 + NEGA(J)%endop,scal*NEGA(J)%PDX,scal*NEGA(J)%PDY,scal*NEGA(J)%PDZ 
  END DO

  CLOSE(UNIT=21)

END SUBROUTINE DOSNE

SUBROUTINE DOSAD(step)
  USE DOUBLE
  USE acommondata, ONLY: ndop, cellc, npartd, ldop, scal 
  USE arrays1,     ONLY: NEGA, NEUT
  USE POTENTIALS,  ONLY: COULOMBALLAD, DOPELECT_POTALLAD
  INTEGER        :: J, step 
  REAL (KIND=DP) :: Vf1R, Vf2R
  CHARACTER(len=1024) :: filename
  print*, 'step', step
  WRITE (filename, "(I5,A4, f3.2, A1, I2, A4)") step, "dosa",ldop,"l",cellc, ".dat"
  OPEN (UNIT=20, FILE=filename, STATUS='NEW', ACTION='WRITE')


   DO J = 1, npartd - 2*ndop    !Remove charge at several cores 
      NEGA(ndop + 1)               = NEUT(J)
      NEGA(ndop + 1)%Zch           = -1       !Charge the former neutral 
!      flu(NEGA(ndop + 1)%site)%Zch = -1       !Charge also the site in flu array  
      CALL COULOMBALLAD(Vf1R)
      CALL DOPELECT_POTALLAD(Vf2R)
!      WRITE(20,*) Vf1R + Vf2R
      WRITE(20,'(F14.8,1x,F12.6,1x,F12.6,1x,F12.6)') Vf1R + Vf2R,scal*NEUT(J)%PDX,scal*NEUT(J)%PDY,scal*NEUT(J)%PDZ
    END DO
    NEGA(ndop + 1)%Zch = 0
 
    CLOSE(UNIT=20)
    
END SUBROUTINE DOSAD

SUBROUTINE DOSRE(step)
  USE DOUBLE
  USE acommondata, only: ndop, cellc, ldop, scal
  USE arrays1, ONLY: NEGA
  USE POTENTIALS, ONLY: COULOMBALLRE, DOPELECT_POTALLRE, COULOMBALL, DOPELECT_POTALL
  INTEGER :: J, step
  REAL (KIND=DP) :: Vf1R, Vf2R
  CHARACTER(len=1024) :: filename
  CHARACTER(len=1024) :: filename2
  
  WRITE(filename, "(I5, A4, f3.2, A1, I2, A4)") step, "dosr", ldop, "l", cellc, ".dat"
  OPEN (UNIT=27, FILE=filename, status='NEW', ACTION='WRITE')
  WRITE(filename2, "(I5, A4, f3.2, A1, I2, A4)") step, "dosn", ldop, "l",cellc, ".dat"
  OPEN (UNIT=17, FILE=filename2, status='NEW', ACTION='WRITE') 

  CALL COULOMBALL(Vf1R)
  CALL DOPELECT_POTALL(Vf2R)
  WRITE(17, '(F14.8)') Vf1R + Vf2R
 ! WRITE(17,*) Vf1R +  Vf2R
  DO J = 1, ndop
     NEGA(J)%Zch = 0
     CALL COULOMBALLRE(Vf1R)
     CALL DOPELECT_POTALLRE(Vf2R)
!     WRITE(27,*) Vf1R + Vf2R
     WRITE (27,'(F14.8,1x,F12.6,1x,F12.6,1x,F12.6)') Vf1R + Vf2R,scal*NEGA(J)%PDX, scal*NEGA(J)%PDY,scal*NEGA(J)%PDZ
     NEGA(J)%Zch = -1
  END DO

 CLOSE(UNIT=17)
 CLOSE(UNIT=27) 

END SUBROUTINE DOSRE
