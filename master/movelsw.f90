SUBROUTINE MOVELSW() 
        USE DOUBLE
        USE arrays1, ONLY: NEGA, NEUT, flu
        USE RANDOM
        USE acommondata, ONLY: ndop, invtemp,  szcell, cellc 
        USE POTENTIALS
        USE CLASS_FLUORS
        IMPLICIT NONE 
        INTEGER        :: I, try 
        REAL (KIND=DP) :: Vf2R, Vf1R, Ediff, old
!        REAL (KIND=DP) :: x 
        REAL (kiND=DP) :: randpos, mcstep
        TYPE (fluors)  :: tempflu1, tempflu2

        DO I = 1, ndop
           CALL RANDOM_NUMBER(randpos)
           try = SIZE(NEUT)*randpos+1  !Try among all possible
           CALL COULOMB(I, Vf1R)       !Energy between pair of electrons
           CALL DOPELECT_POT(I, Vf2R)  !Energy between electron and all dopes by defined potential
!           x = NEGA(I)%PDX
           old       = Vf1R + Vf2R
           tempflu1  = NEGA(I)         !Save temporaly the charged  
           tempflu2  = NEUT(try)       !Save temporaly the neutrum
           NEUT(try)%Zch = -1             !Charging
!           NEGA(I)%Zch   = 0              !Neutralize
           NEGA(I)   = NEUT(try)          !Living the charged C60
           NEUT(try) = tempflu1           !Arriving neutral C60
           NEUT(try)%Zch = 0              !Adjust charge
           CALL COULOMB(I, Vf1R)       !Energy energy between pair of electrons 
           CALL DOPELECT_POT(I, Vf2R)  !Energy between electron and all dopes by defined potential
!           x = x - NEGA(I)%PDX
!           x = x - ANINT(x)  
           Ediff          = old - Vf1R - Vf2R !+ efield*scal*x 
           mcstep         = rannyu()
           IF ((Ediff .GE. 0.d0) .OR. (mcstep .LE.  exp(invtemp*Ediff))) THEN
               flu(NEGA(I)%site)%Zch   = -1  !New
               flu(NEUT(try)%site)%Zch =  0  !New
!                print*,  szcell*cellc*DISTANCE(x, y, z), old, fact*Vf1R + Vf2R, old - fact*Vf1R - Vf2R 
           ELSE                         !Return values to the originals: charge, level
                NEGA(I)   = tempflu1
                NEUT(try) = tempflu2
           END IF  
        END DO

END SUBROUTINE MOVELSW 
