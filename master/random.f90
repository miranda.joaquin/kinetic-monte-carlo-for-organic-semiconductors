MODULE RANDOM
       !Portable random generator
CONTAINS


function rannyu()
     USE DOUBLE
     USE rnyucm
     IMPLICIT NONE
!     INTEGER i
     REAL (KIND=DP) :: twom12, rannyu
     PARAMETER(twom12=0.000244140625d0)

      i1=l(1)*m(4)+l(2)*m(3)+l(3)*m(2)+l(4)*m(1)
      i2=l(2)*m(4)+l(3)*m(3)+l(4)*m(2)
      i3=l(3)*m(4)+l(4)*m(3)
      i4=l(4)*m(4)
      l(4)=mod(i4,4096)
      i3=i3+i4/4096
      l(3)=mod(i3,4096)
      i2=i2+i3/4096
      l(2)=mod(i2, 4096)
      l(1)=mod(i1+i2/4096, 4096)
      rannyu=twom12*(REAl(l(1)) + twom12*(REAL(l(2)) + twom12*(REAL(l(3)) + twom12*REAL(l(4)))))
      return
end function rannyu

subroutine setrn(iseed)
      USE rnyucm
      USE DOUBLE

      INTEGER iseed(4), i

       do  i=1,4
          l(i)=mod(iseed(i),4096)
       end do
          l(4)=2*(l(4)/2)+1

! shift everything to the left if not 48 bit

      if (nbit.lt.48) then
         do i=1,48-nbit
         i1=l(1)*2
         i2=l(2)*2
         i3=l(3)*2
         i4=l(4)*2
         l(4)=mod(i4, 4096)
         i3=i3+i4/4096
         l(3)=mod(i3,4096)
         i2=i2+i3/4096
         l(2)=mod(i2, 4096)
         l(1)=mod(i1+i2/4096,4096)
         end do
         endif
      return
end subroutine setrn


END MODULE RANDOM
