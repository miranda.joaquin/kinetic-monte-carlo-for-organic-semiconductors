MODULE acommondata

    USE DOUBLE
    IMPLICIT NONE

    SAVE
    INTEGER, PARAMETER :: cellc   = 20    !Lengh of box (number of cells in one direction) 
    REAL, PARAMETER    :: ldop    = 0.01  !Percentage of doping
    INTEGER, PARAMETER :: nmc     = 5000  !Number of MC steps
    REAL, PARAMETER    :: tempera = 300  
    REAL (KIND=DP), PARAMETER :: convl   =  0.529177  !Lenght conversion of atomic units (Bohr)
    REAL (KIND=DP), PARAMETER :: convHeV = 27.21138386!Atomic energy conversion (Hartrees)
    REAL (KIND=DP) :: VR                              !Energy fix background,dopant-dopant interaction
    REAL (KIND=DP) :: DF(1:4) 
    REAL (KIND=DP), PARAMETER :: szcell = 10*sqrt(2.d0)!Size of the FCC cell, 10A is C60-C60 distance
    INTEGER :: iseed(4)                   
    INTEGER :: npartd                                  !Total number os cites
    INTEGER :: ndop    = ldop*4*cellc*cellc*cellc      !doping sites for a (4* for FCC) 
    INTEGER :: listtry = 0.4*4*cellc*cellc*cellc       !try sites for dopants for the FCC
    REAL (KIND=DP) :: dist, dir, mcstep
    REAL (KIND=DP) :: scal =  szcell*cellc
!!  *********************************************************************************
!!  *******parameters for the DFT-based electron-dopt potential**********************
!!    -en0 + (en0 -  (convl* convHeV)/(epsble*RIJ))/(1 + exp((bb-RIJ)/sig))  
    REAL (KIND=DP), PARAMETER :: epsdop = 2.0d0
    REAL (KIND=DP), PARAMETER :: epsc60 = 4.4d0
    REAL, PARAMETER           :: epsble = epsdop*ldop + (1-ldop)*epsc60 ! 4.4
    REAL (KIND=DP), PARAMETER :: bb     = 9.1d0
    REAL (KIND=DP), PARAMETER :: sig    = 1.25d0
    REAL (KIND=DP)            :: en0    = 0.634*(epsc60/epsble)
!!  *********************************************************************************
    REAL (KIND=DP)            :: fact = convHeV*convl/epsble
    REAL (KIND=DP), PARAMETER :: kB = 0.000086173303
    REAL, PARAMETER           :: evtemp  = kB*tempera     !Convert to eV. T = 300K -> 0.02585 eV
    REAL (KIND=DP), PARAMETER :: invtemp = 1/evtemp       !(1/KbT)
    REAL (KIND=DP), PARAMETER :: LkT     = 0.076d0*evtemp ! Lambda60*kbT)
    REAL (KIND=DP), PARAMETER :: unitevfreq = sqrt(1/LkT)*0.000625d0 
    DATA       iseed/1,2,1,0/
    REAL (KIND=DP), PARAMETER :: femtos = 1e15            !Time scale for kMC
    REAL (KIND=DP)            :: tottime, totvx, totdx 
    REAL (KIND=DP), PARAMETER :: efield = -0.001d0 
    REAL (KIND=DP)            :: s2phbr = sqrt(2*3.14159265358979323846264338327950288419)/(6.5821195141*1e-16*1e15)
!    hbar = 6.58211899e-16
    CONTAINS

    FUNCTION  DISTANCE(XIJ, YIJ, ZIJ)
       USE DOUBLE
       IMPLICIT NONE
       REAL (KIND=DP) :: XIJ, YIJ, ZIJ, DISTANCE

       XIJ = XIJ - ANINT( XIJ )
       YIJ = YIJ - ANINT( YIJ )
       ZIJ = ZIJ - ANINT( ZIJ )
       DISTANCE = sqrt(XIJ*XIJ + YIJ*YIJ+ ZIJ*ZIJ) 
    END FUNCTION DISTANCE

END MODULE
