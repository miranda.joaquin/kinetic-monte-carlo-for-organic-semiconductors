MODULE arrays1
        USE CLASS_FLUORS
        USE CLASS_FREQS
        IMPLICIT NONE
        SAVE  
        
        INTEGER, ALLOCATABLE      :: res(:)
        INTEGER, ALLOCATABLE      :: ITRY(:)
        TYPE(fluors), ALLOCATABLE :: flu(:)
        TYPE(fluors), ALLOCATABLE :: NEGA(:), HOLE(:), NEUT(:)
        TYPE(sumratios)           :: freqs
!        TYPE(sumratios), ALLOCATABLE :: freqs(1:30) 
        !SAVE

END MODULE arrays1
